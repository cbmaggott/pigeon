package com.wacktec.pigeon.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/")
@RestController
public class Home {
	@GetMapping
	public String test() {
		return "Pigeon testing!";
	}
}
